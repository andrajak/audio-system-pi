file(REMOVE_RECURSE
  "../lib/libiimav.pdb"
  "../lib/libiimav.so"
  "../lib/libiimav.so.1"
  "../lib/libiimav.so.1.1.0"
  "CMakeFiles/iimav.dir/AlsaDevice.cpp.o"
  "CMakeFiles/iimav.dir/AlsaSink.cpp.o"
  "CMakeFiles/iimav.dir/AlsaSource.cpp.o"
  "CMakeFiles/iimav.dir/AudioFilter.cpp.o"
  "CMakeFiles/iimav.dir/AudioSink.cpp.o"
  "CMakeFiles/iimav.dir/AudioTypes.cpp.o"
  "CMakeFiles/iimav.dir/SDLDevice.cpp.o"
  "CMakeFiles/iimav.dir/Utils.cpp.o"
  "CMakeFiles/iimav.dir/WaveFile.cpp.o"
  "CMakeFiles/iimav.dir/WaveSink.cpp.o"
  "CMakeFiles/iimav.dir/WaveSource.cpp.o"
  "CMakeFiles/iimav.dir/artnet/ARTNet.cpp.o"
  "CMakeFiles/iimav.dir/artnet/DatagramSocket.cpp.o"
  "CMakeFiles/iimav.dir/artnet/Socket.cpp.o"
  "CMakeFiles/iimav.dir/filters/AllPassFilter.cpp.o"
  "CMakeFiles/iimav.dir/filters/CombFilter.cpp.o"
  "CMakeFiles/iimav.dir/filters/Delay.cpp.o"
  "CMakeFiles/iimav.dir/filters/Distortion.cpp.o"
  "CMakeFiles/iimav.dir/filters/Echo.cpp.o"
  "CMakeFiles/iimav.dir/filters/FIRFilter.cpp.o"
  "CMakeFiles/iimav.dir/filters/Fuzz.cpp.o"
  "CMakeFiles/iimav.dir/filters/NullFilter.cpp.o"
  "CMakeFiles/iimav.dir/filters/Overdrive.cpp.o"
  "CMakeFiles/iimav.dir/filters/Reverb.cpp.o"
  "CMakeFiles/iimav.dir/filters/ReverbByEcho.cpp.o"
  "CMakeFiles/iimav.dir/filters/SineMultiply.cpp.o"
  "CMakeFiles/iimav.dir/filters/Volume.cpp.o"
  "CMakeFiles/iimav.dir/video_ops.cpp.o"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/iimav.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()

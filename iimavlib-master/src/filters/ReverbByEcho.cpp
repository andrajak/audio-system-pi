#include "iimavlib/filters/ReverbByEcho.h"
#include "iimavlib/Utils.h"
#include <algorithm>

namespace iimavlib {

	ReverbByEcho::ReverbByEcho(const pAudioFilter& child, double delay, double decay)
		:AudioFilter(child), delay1_(delay), delay2_(delay*0.33), delay3_(delay*0.11), decay_(decay)
	{

	}
	ReverbByEcho::~ReverbByEcho()
	{

	}
	namespace {
		template<typename T>
		void add_echo(T dest, T src, size_t count, double decay)
		{
			for (size_t sample = 0; sample < count; ++sample) {
				*dest = (decay * *src++) + ((1.0 - decay) * *dest);
				dest++;
			}
		}
	}
	void ReverbByEcho::solveRestOfSamples(audio_buffer_t& buffer, const size_t& delay_samples, std::vector<audio_sample_t>& old_samples, const size_t& from_old, const double& decay)
	{
		if (buffer.valid_samples <= delay_samples) {
			// Move unused part of the buffer to the beginning
			std::copy(old_samples.begin() + from_old, old_samples.end(), old_samples.begin());
			// Resize old_samples_ to the size of valid data in it
			old_samples.resize(std::distance(old_samples.begin() + from_old, old_samples.end()));
			// And insert samples from processed buffer
			old_samples.insert(old_samples.end(), buffer.data.begin(), buffer.data.begin() + buffer.valid_samples);
		}
		else {
			// Add echo to the rest of input buffer
			add_echo(buffer.data.begin() + from_old, buffer.data.begin(), (buffer.valid_samples - from_old), decay);
			// We have no valid old samples
			old_samples.resize(0);
			// Copy samples to old_samples_ from input buffer
			old_samples.insert(old_samples.end(),
				buffer.data.begin() + (buffer.valid_samples - delay_samples),
				buffer.data.begin() + buffer.valid_samples);
		}
	}
	error_type_t ReverbByEcho::do_process(audio_buffer_t& buffer)
	{

		// Return OK for empty buffer - nothing to do here
		if (buffer.valid_samples == 0) return error_type_t::ok;

		// Some constant values that is convenient to have prepared
		const size_t frequency = convert_rate_to_int(buffer.params.rate);
		const size_t delay_samples1 = static_cast<size_t>(frequency * delay1_);
		const size_t delay_samples2 = static_cast<size_t>(frequency * delay2_);
		const size_t delay_samples3 = static_cast<size_t>(frequency * delay3_);

		// Make sure old_samples_ is large enough
		old_samples1_.resize(delay_samples1, 0);
		old_samples2_.resize(delay_samples2, 0);
		old_samples3_.resize(delay_samples3, 0);

		// Pointer to the buffer after type conversion to int16_t
		const auto data = buffer.data.begin();

		// Calculate how many samples from old_samples we're gonna use
		const size_t from_old1 = std::min(buffer.valid_samples, delay_samples1);
		const size_t from_old2 = std::min(buffer.valid_samples, delay_samples2);
		const size_t from_old3 = std::min(buffer.valid_samples, delay_samples3);

		// And add echo to them (from old_samples_)
		add_echo(data, old_samples1_.begin(), from_old1, decay_);
		add_echo(data, old_samples2_.begin(), from_old2, decay_/3);
		add_echo(data, old_samples3_.begin(), from_old3, decay_/9);

		// echo 1, 2 and 3 ...
		// if buffer.valid_samples is lesser or equal than delay_samples, we already have processed all the samples
		solveRestOfSamples(buffer, delay_samples1, old_samples1_, from_old1, decay_);
		solveRestOfSamples(buffer, delay_samples2, old_samples2_, from_old2, decay_/3);
		solveRestOfSamples(buffer, delay_samples3, old_samples3_, from_old3, decay_/9);
		
		return error_type_t::ok;
	}
}


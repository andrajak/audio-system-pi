#include "iimavlib/filters/Overdrive.h"
#include "iimavlib/Utils.h"
#include <algorithm>
#include <limits.h>

namespace iimavlib
{

    Overdrive::Overdrive(const pAudioFilter &child, int16_t max_value)
        : AudioFilter(child), max_value_(max_value)
    {
        part0_ = max_value_ / 50;
        part1_ = max_value_ / 3;
        part2_ = (max_value_ / 3) * 2;
    }

    Overdrive::~Overdrive() {}

    // count output value of second part of function
    int16_t countPart2Value(int16_t value, int16_t max_value)
    {
        float x = ((float) value) / max_value;
        return (int16_t)(((3 - (2 - 3 * x) * (2 - 3 * x)) / 3) * max_value);
    }

    int16_t countPart2NegativeValue(int16_t value, int16_t max_value)
    {
        float x = ((float) value) / max_value;
        return (int16_t)(((3 - (2 + 3 * x) * (2 + 3 * x)) / 3) * (-max_value));
    }

    void Overdrive::add_overdrive(std::vector<audio_sample_t>::iterator dest, size_t samples)
    {
        for (size_t sample = 0; sample < samples; ++sample)
        {
            // first part of function
            if (*dest > -part1_ && *dest < part1_)
            {
                (*dest).setLeft((*dest).getLeft() * 2);
                (*dest).setRight((*dest).getRight() * 2);
            }     
            // second part of function
            else if (*dest > -part2_ && *dest < part2_)
            {
                if (*dest > 0) {
                    (*dest).setLeft(countPart2Value((*dest).getLeft(), max_value_));
                    (*dest).setRight(countPart2Value((*dest).getRight(), max_value_));
                }
                else {
                    (*dest).setLeft(countPart2NegativeValue((*dest).getLeft(), max_value_));
                    (*dest).setRight(countPart2NegativeValue((*dest).getRight(), max_value_));
                }
            }
            // third part of function
            else if (*dest > part2_)
            {
                (*dest).setLeft(max_value_);
                (*dest).setRight(max_value_);
            }
            // third part of function for negative values
            else 
            {
                (*dest).setLeft(-max_value_);
                (*dest).setRight(-max_value_);
            }

            dest++;
        }
    }

    error_type_t Overdrive::do_process(audio_buffer_t &buffer)
    {
        // Return OK for empty buffer - nothing to do here
        if (buffer.valid_samples == 0)
            return error_type_t::ok;

        add_overdrive(buffer.data.begin(), buffer.valid_samples);

        return error_type_t::ok;
    }

}
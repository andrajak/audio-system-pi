#include "iimavlib/filters/AllPassFilter.h"

namespace iimavlib
{

  AllpassFilter::AllpassFilter(size_t delay, float gain)
  {

    delay_ = delay;
    gain_ = gain;
    buffer_ = std::make_unique<deque>(delay_, 0.0);
  }

  audio_sample_t AllpassFilter::do_filtering(audio_sample_t x)
  {
    auto &z = *buffer_.get();
    auto z_out = z.back();
    z.pop_back();

    auto z_in = x + -gain_ * z_out;
    z.push_front(z_in);
    auto y = gain_ * z_in + z_out;

    return y;
  }

}
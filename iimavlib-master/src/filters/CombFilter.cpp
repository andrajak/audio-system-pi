#include "iimavlib/filters/CombFilter.h"

namespace iimavlib {

CombFilter::CombFilter(size_t delay, float gain) {

  delay_ = delay;
  gain_ = gain;

  buffer_ = std::make_unique<deque>(delay_, 0.0);
}

audio_sample_t CombFilter::do_filtering(audio_sample_t x) {
  auto &d = *buffer_.get();

  auto d_out = d.back();
  d.pop_back();

  auto d_in = x + gain_*d_out;
  d.push_front(d_in);

  return d_out;
}

}
#include "iimavlib/filters/Fuzz.h"
#include "iimavlib/Utils.h"
#include <algorithm>
#include <stdlib.h>

namespace iimavlib
{

	Fuzz::Fuzz(const pAudioFilter &child, int16_t threshold, int16_t maxValue)
		: AudioFilter(child), threshold_(threshold), maxValue_(maxValue) {}

	Fuzz::~Fuzz() {}

	void Fuzz::add_fuzz(std::vector<audio_sample_t>::iterator dest, size_t samples)
	{
		for (size_t sample = 0; sample < samples; ++sample)
		{
			if (*dest > threshold_)
			{
				(*dest).setLeft(maxValue_);
				(*dest).setRight(maxValue_);
			}
			else if (*dest < -threshold_)
			{
				(*dest).setLeft(-maxValue_);
				(*dest).setRight(-maxValue_);
			}
			dest++;
		}
	}

	error_type_t Fuzz::do_process(audio_buffer_t &buffer)
	{
		// Return OK for empty buffer - nothing to do here
		if (buffer.valid_samples == 0)
			return error_type_t::ok;

		add_fuzz(buffer.data.begin(), buffer.valid_samples);

		return error_type_t::ok;
	}
}
#include "iimavlib/filters/Reverb.h"
#include "iimavlib/Utils.h"
#include <algorithm>
#include <math.h>

namespace iimavlib
{
	size_t countAllpassDelay(int i, size_t sample_rate)
	{
		return (0.1 * sample_rate) / pow(3, i);
	}

	// at least 200 ms needed, so (sample rate / 5) 
	size_t countBaseDelay(size_t sample_rate) {
		return sample_rate / 5;
	}

	size_t countBonusDelay(size_t bonus_delay, size_t sample_rate) {
		return (sample_rate / 500) * bonus_delay;
	}

	Reverb::Reverb(const pAudioFilter &child, size_t bonus_delay, float wet, size_t sample_rate)
		: AudioFilter(child),
		  allpass_f0_(countAllpassDelay(0, sample_rate) 
		  	+ countBonusDelay(bonus_delay, sample_rate), -0.7),
		  allpass_f1_(countAllpassDelay(1, sample_rate)
		  	+ countBonusDelay(bonus_delay, sample_rate) / 3, 0.7),
		  allpass_f2_(countAllpassDelay(2, sample_rate)
		  	+ countBonusDelay(bonus_delay, sample_rate) / 9, -0.6),
		  allpass_f3_(countAllpassDelay(3, sample_rate), 0.6)
	{
		base_delay_ = countBaseDelay(sample_rate);
		base_delay_half_ = base_delay_ / 2;
		base_delay_third_ = base_delay_ / 3;
		delay_ = std::make_unique<std::deque<audio_sample_t>>
			(base_delay_ + countBonusDelay(bonus_delay, sample_rate), 0.0);

		if (wet >= 0 && wet <= 1)
			wet_ = wet;
		else
			wet_ = 0.5;
	}
	
	Reverb::~Reverb()
	{
	}

	audio_sample_t Reverb::add_reverb(audio_sample_t sample_x)
	{
		auto y0 = allpass_f0_.do_filtering(sample_x);
		auto y1 = allpass_f1_.do_filtering(y0);
		auto y2 = allpass_f2_.do_filtering(y1);
		auto y3 = allpass_f3_.do_filtering(y2);

		return wet_ * y3 + (1 - wet_) * sample_x;
	}

	error_type_t Reverb::do_process(audio_buffer_t &buffer)
	{

		// return OK for empty buffer - nothing to do here
		if (buffer.valid_samples == 0)
			return error_type_t::ok;

		for (auto it = buffer.data.begin(); it != buffer.data.end(); ++it)
		{
			*it = add_reverb(*it);
		}

		return error_type_t::ok;
	}
}

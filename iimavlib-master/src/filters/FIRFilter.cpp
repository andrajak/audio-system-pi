#include "iimavlib/filters/FIRFilter.h"

namespace iimavlib {

FIRFilter::FIRFilter(std::vector<float> taps) : taps_(taps) {
  
  delay_ = taps.size();
  input_samples_ = std::make_unique<deque>(delay_, 0.0);
}

audio_sample_t FIRFilter::do_filtering(audio_sample_t x) {
  auto &g = *input_samples_.get();

  g.pop_back();
  g.push_front(x);

  audio_sample_t new_val = 0;
  for (size_t i = 0; i < delay_; ++i) {
    new_val += taps_[i]*g[i];
  }

  return new_val;
}

}
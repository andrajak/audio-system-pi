#include "iimavlib/filters/Distortion.h"
#include "iimavlib/Utils.h"
#include <algorithm>

namespace iimavlib {

	Distortion::Distortion(const pAudioFilter& child, int16_t distortion_value)
		:AudioFilter(child), distortion_value_(distortion_value) {}

	Distortion::~Distortion() {}

	namespace {
		template<typename T>
		void add_distortion(T dest, size_t samples, int16_t distortion_value)
		{
			for (size_t sample = 0; sample < samples; ++sample) {
				if (*dest > distortion_value) {
					(*dest).setLeft(distortion_value);
					(*dest).setRight(distortion_value);
				}
				else if (*dest < -distortion_value) {
					(*dest).setLeft(-distortion_value);
					(*dest).setRight(-distortion_value);
				}
				dest++;
			}
		}
	}
	error_type_t Distortion::do_process(audio_buffer_t& buffer)
	{
		// Return OK for empty buffer - nothing to do here
		if (buffer.valid_samples == 0) return error_type_t::ok;

		add_distortion(buffer.data.begin(), buffer.valid_samples, distortion_value_);

		return error_type_t::ok;
	}
}
#include "iimavlib/filters/Volume.h"
#include "iimavlib/Utils.h"
#include <algorithm>

namespace iimavlib {

	Volume::Volume(const pAudioFilter& child, double volume_value)
		:AudioFilter(child), volume_value_(volume_value) {}

	Volume::~Volume() {}

	namespace {
		template<typename T>
		void change_volume(T dest, size_t samples, double volume_value)
		{
			for (size_t sample = 0; sample < samples; ++sample) {
				(*dest).setLeft((*dest).getLeft() * (volume_value / 100));
                (*dest).setRight((*dest).getRight() * (volume_value / 100));
				dest++;
			}
		}
	}
	error_type_t Volume::do_process(audio_buffer_t& buffer)
	{
		// Return OK for empty buffer - nothing to do here
		if (buffer.valid_samples == 0) return error_type_t::ok;

		change_volume(buffer.data.begin(), buffer.valid_samples, volume_value_);

		return error_type_t::ok;
	}
}
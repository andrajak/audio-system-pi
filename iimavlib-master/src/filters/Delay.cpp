#include "iimavlib/filters/Delay.h"
#include "iimavlib/Utils.h"
#include <algorithm>

namespace iimavlib {

	Delay::Delay(const pAudioFilter& child, double decay, double delay)
		:AudioFilter(child), decay_(decay), delay_(delay) {}

	Delay::~Delay() {}

	namespace {
		template<typename T>
		void add_delay(T dest, T src, size_t samples, double decay) {
			for (size_t sample = 0; sample < samples; ++sample) {
				*dest = (decay * *src++) + ((1.0-decay)* *dest);
				dest++;
			}
		}
	}
	error_type_t Delay::do_process(audio_buffer_t& buffer)
	{
		// Return OK for empty buffer - nothing to do here
		if (buffer.valid_samples==0) return error_type_t::ok;

		// Some constant values that is convenient to have prepared
		const size_t frequency = convert_rate_to_int(buffer.params.rate);
		const size_t delay_samples = static_cast<size_t>(frequency*delay_);

		// Make sure old_samples_ is large enough
		old_samples_.resize(delay_samples,0);

		const auto data = buffer.data.begin();

		// Calculate how many samples from old_samples we're gonna use
		const size_t from_old = std::min(buffer.valid_samples, delay_samples);

		audio_buffer_t tmp = buffer;
		add_delay(data, old_samples_.begin(), from_old, decay_);

		// if buffer.valid_samples is lesser or equal than delay_samples, we already have processed all the samples
		if (buffer.valid_samples <= delay_samples) {
			// Move unused part of the buffer to the beginning
			std::copy(old_samples_.begin()+from_old,old_samples_.end(),old_samples_.begin());
			// Resize old_samples_ to the size of valid data in it
			old_samples_.resize(std::distance(old_samples_.begin()+from_old,old_samples_.end()));
			// And insert samples from processed buffer
			old_samples_.insert(old_samples_.end(), tmp.data.begin(), tmp.data.begin()+tmp.valid_samples);
		} else {
			add_delay(data+from_old,buffer.data.begin(),(buffer.valid_samples-from_old),decay_);
			// We have no valid old samples
			old_samples_.resize(0);
			// Copy samples to old_samples_ from input buffer
			old_samples_.insert(old_samples_.end(),
							data+(buffer.valid_samples-delay_samples),
							data+buffer.valid_samples);
		}
		return error_type_t::ok;
	}
}
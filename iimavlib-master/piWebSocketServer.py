#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  piServer.py
#
from socket import *
from time import ctime
import os

HOST = ''
PORT = 22570
BUFSIZE = 1024
ADDR = (HOST,PORT)

tcpSerSock = socket(AF_INET, SOCK_STREAM)
tcpSerSock.bind(ADDR)
tcpSerSock.listen(2)

while True:
    print('...Waiting for connection')
    tcpCliSock, addr = tcpSerSock.accept()
    print(addr)
    try:
        while True:
            data = ''
            data = tcpCliSock.recv(BUFSIZE)
            if not data:
                break
            cmdInput = data.decode().split(':')
            print(cmdInput[0])
            
            if cmdInput[0] == 'stop':
                pgid = os.getpgid(os.getpid())
                if pgid == 1:
                    os.kill(os.getpid(), signal.SIGINT)
                else:
                    os.killpg(os.getpgid(os.getpid()), signal.SIGINT)
            if cmdInput[1] == 'true':
                os.system('./build/bin/empty_project ' + './build/sweetDreams_16bit.wav ' + '-r ' + cmdInput[0])
            else:
                os.system('./build/bin/empty_project ' + './build/sweetDreams_16bit.wav ' + cmdInput[0])
    except KeyboardInterrupt:
        print('...Keyboard Interrupt ')
    tcpCliSock.close()
tcpSerSock.close()

        


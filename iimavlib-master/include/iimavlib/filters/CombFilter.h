#ifndef COMB_FILTER_H_
#define COMB_FILTER_H_

#include <memory>
#include <deque>

#include "../AudioFilter.h"

namespace iimavlib {
class CombFilter {
using deque = std::deque<audio_sample_t>;
using buffPtr = std::unique_ptr<deque>;

public:
  CombFilter(size_t delay, float gain);
  ~CombFilter() = default;

  audio_sample_t do_filtering(audio_sample_t x);

private:
  size_t delay_;
  float gain_;
  buffPtr buffer_;
};

}

#endif /* COMB_FILTER_H_ */
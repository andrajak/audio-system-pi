#ifndef REVERBBYECHO_H_
#define REVERBBYECHO_H_

#include "../AudioFilter.h"
namespace iimavlib {
	class EXPORT ReverbByEcho : public AudioFilter {
	public:
		ReverbByEcho(const pAudioFilter& child, double delay = 0.3, double decay = 0.4);
		virtual ~ReverbByEcho();
	private:
		virtual error_type_t do_process(audio_buffer_t& buffer);
		std::vector<audio_sample_t> old_samples1_;
		std::vector<audio_sample_t> old_samples2_;
		std::vector<audio_sample_t> old_samples3_;
		double delay1_;
		double delay2_;
		double delay3_;
		double decay_;

		void solveRestOfSamples(audio_buffer_t& buffer, const size_t& delay_samples, 
		std::vector<audio_sample_t>& old_samples, const size_t& from_old, const double& decay);
	};
}


#endif /* REVERBBYECHO_H_ */


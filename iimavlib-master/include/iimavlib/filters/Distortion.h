#ifndef DISTORTION_H_
#define DISTORTION_H_

#include "../AudioFilter.h"

namespace iimavlib {
	class EXPORT Distortion : public AudioFilter {
	public:
		Distortion(const pAudioFilter& child, int16_t distortion_value_ = 420);
		virtual ~Distortion();
	private:
		virtual error_type_t do_process(audio_buffer_t& buffer);
		int16_t distortion_value_;
	};

}


#endif /* DISTORTION_H_ */


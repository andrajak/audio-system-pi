#ifndef REVERB_H_
#define REVERB_H_

#include <cstdint>
#include <memory>
#include <deque>
#include "CombFilter.h"
#include "AllPassFilter.h"
#include "FIRFilter.h"

using std::size_t;
using std::uint8_t;
using std::int16_t;
using std::array;

#include "../AudioFilter.h"
namespace iimavlib {
	class EXPORT Reverb : public AudioFilter {
	public:
		Reverb(const pAudioFilter& child, size_t bonus_delay = 0, float wet = 0.3, size_t sample_rate = 192000);
		virtual ~Reverb();
	private:
  		AllpassFilter allpass_f0_;
		AllpassFilter allpass_f1_;
		AllpassFilter allpass_f2_;
		AllpassFilter allpass_f3_;
		std::unique_ptr<std::deque<audio_sample_t>> delay_;
		
		float wet_;
		size_t bonus_delay_;

		size_t base_delay_;
		size_t base_delay_half_;
		size_t base_delay_third_;

		virtual error_type_t do_process(audio_buffer_t& buffer);
		audio_sample_t add_reverb(audio_sample_t sample_x);
	};

}


#endif /* REVERB_H_ */
#ifndef FUZZ_H_
#define FUZZ_H_

#include "../AudioFilter.h"

namespace iimavlib {
	class EXPORT Fuzz : public AudioFilter {
	public:
		Fuzz(const pAudioFilter& child, int16_t threshold_ = 300, int16_t maxValue_ = 400);
		virtual ~Fuzz();
	private:
		virtual error_type_t do_process(audio_buffer_t& buffer);
		void add_fuzz(std::vector<audio_sample_t>::iterator dest, size_t samples);
		int16_t threshold_;
        int16_t maxValue_;
	};

}


#endif /* FUZZ_H_ */
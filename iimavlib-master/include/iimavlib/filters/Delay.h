#ifndef DELAY_H_
#define DELAY_H_

#include "../AudioFilter.h"

namespace iimavlib {
	class EXPORT Delay : public AudioFilter {
	public:
		Delay(const pAudioFilter& child, double decay=0.5, double delay=0.5);
		virtual ~Delay();
	private:
		virtual error_type_t do_process(audio_buffer_t& buffer);
		std::vector<audio_sample_t> old_samples_;
		double delay_;
		double decay_;
	};

}

#endif /* DELAY_H_ */

#ifndef FIR_FILTER_H_
#define FIR_FILTER_H_

#include <memory>
#include <deque>
#include <vector>

using std::size_t;
using std::int32_t;

using std::array;

#include "../AudioFilter.h"

namespace iimavlib {
class FIRFilter {
using deque = std::deque<audio_sample_t>;
using buffPtr = std::unique_ptr<deque>;

public:
  FIRFilter(std::vector<float> taps);
  ~FIRFilter() = default;

  audio_sample_t do_filtering(audio_sample_t x);

private:

  size_t delay_;
  std::vector<float> taps_;
  buffPtr input_samples_;
};

}

#endif /* FIR_FILTER_H_ */
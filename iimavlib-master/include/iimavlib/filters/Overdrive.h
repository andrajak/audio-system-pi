#ifndef OVERDRIVE_H_
#define OVERDRIVE_H_

#include "../AudioFilter.h"

namespace iimavlib {
	class EXPORT Overdrive : public AudioFilter {
	public:
		Overdrive(const pAudioFilter& child, int16_t max_value);
		virtual ~Overdrive();
	private:
		virtual error_type_t do_process(audio_buffer_t& buffer);
		void add_overdrive(std::vector<audio_sample_t>::iterator dest, size_t samples);
        int16_t max_value_;
        int16_t part0_, part1_, part2_;
	};

}

#endif /* OVERDRIVE_H_ */
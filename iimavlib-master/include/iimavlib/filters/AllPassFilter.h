#ifndef ALLPASS_FILTER_H_
#define ALLPASS_FILTER_H_

#include <memory>
#include <deque>

#include "../AudioFilter.h"

namespace iimavlib {
class AllpassFilter {
using deque = std::deque<audio_sample_t>;
using buffPtr = std::unique_ptr<deque>;

public:
  AllpassFilter(size_t delay, float gain);
  ~AllpassFilter() = default;

  audio_sample_t do_filtering(audio_sample_t x);

private:
  size_t delay_;
  float gain_;
  buffPtr buffer_;
};

}

#endif /* ALLPASS_FILTER_H_ */
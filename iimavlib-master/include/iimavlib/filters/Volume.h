#ifndef VOLUME_H_
#define VOLUME_H_

#include "../AudioFilter.h"

namespace iimavlib {
	class EXPORT Volume : public AudioFilter {
	public:
		Volume(const pAudioFilter& child, double volume_value_ = 100);
		virtual ~Volume();
	private:
		virtual error_type_t do_process(audio_buffer_t& buffer);
		double volume_value_;
	};

}

#endif /* VOLUME_H_ */
from bluedot.btcomm import BluetoothServer
import signal
import json
import subprocess
import os

# 'ls' only for init purpose
proc = subprocess.Popen(['ls'])
path = '/home/pi/AudioSystemPi/iimavlib-master/build/bin/playthrough'

def data_received(data):
	global proc
	global path
	print(data)
	try:
		jsonData = json.loads(data)
	except Exception as e:
		print("Multiple json object!!")
		return
	settings = ""
	if jsonData['isPedalBoardActive'] == True:
		for effect in jsonData['effects']:
			if effect['isActive'] == False:
				continue
			settings += ' ' + effect['name']
			for parametr in effect['parameters']:
				settings += ' ' + str(parametr['value'])
		cmd = path + settings
		poll = proc.poll()
		if poll is None:
			os.killpg(os.getpgid(proc.pid), signal.SIGTERM)
		proc = subprocess.Popen(cmd, shell=True, preexec_fn=os.setsid)
	else:
		poll = proc.poll()
		if poll is None:
			os.killpg(os.getpgid(proc.pid), signal.SIGTERM)

s = BluetoothServer(data_received)
signal.pause()

/*!
 * @file 		empty_project.cpp
 * @author 		Zdenek Travnicek <travnicek@iim.cz>
 * @date 		6.3.2014
 * @copyright	Institute of Intermedia, CTU in Prague, 2013
 * 				Distributed under BSD Licence, details in file doc/LICENSE
 *
 * Simple example reading a wave file and playing it out using high level API.
 */


#include "iimavlib.h"
#include "iimavlib/WaveSource.h"
#include "iimavlib/filters/Echo.h"
#include "iimavlib/filters/Delay.h"
#include "iimavlib/filters/Volume.h"
#include "iimavlib/filters/Reverb.h"
#include "iimavlib/filters/Distortion.h"
#include "iimavlib/filters/Fuzz.h"
#include "iimavlib/filters/Overdrive.h"
#include "iimavlib/Utils.h"
#include <string>


int main(int argc, char** argv) try
{
	using namespace iimavlib;
	/* ******************************************************************
	 *                Process command line parameters
	 ****************************************************************** */
	if (argc > 1 && (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "help") == 0)) {
		std::cout << "Start exe file with arguments. First argument is input wav file, then pairs of filters and values." << std::endl;
		std::cout << "Filters are:   volume    , -v, [0-100], can be bigger, but be careful" << std::endl;
		std::cout << "               echo      , -e, [0.0-1.5]" << std::endl;
		std::cout << "               reverb    , -r, [0.0-1.5]" << std::endl;
		std::cout << "               distortion, -d, [1000-7000]" << std::endl;
		std::cout << "Shown values are approx, but can not be negative!" << std::endl;
		std::cout << "" << std::endl;
		std::cout << "Example: \\bin\\Release\\file.exe song_16bit.wav -r 0.5 -v 100" << std::endl;
		return 0;
	}

	if (argc<2) {
		logger[log_level::fatal] << "Not enough parameters. Specify the wave file please";
		return 1;
	}
	const std::string filename (argv[1]);
	logger[log_level::debug] << "Loading file " << filename;

	audio_id_t device_id = PlatformDevice::default_device();
	/*if (argc>2) {
		device_id = simple_cast<audio_id_t>(argv[2]);
	}*/
	/* ******************************************************************
	 *                Create and run the filter chain
	 ****************************************************************** */

	//if (argc < 4 || argc % 2 == 1) {
	if (argc < 4) {
		logger[log_level::fatal] << "Specify one or more valid filters please. Use -h for help!";
		logger[log_level::fatal] << argc;
		return 1;
	}

	auto chain = filter_chain<WaveSource>(filename);
	int arg = 2, value = 3;
	while (arg < argc) {
		/*if (strcmp(argv[arg], "Reverb") == 0) {
			chain = chain.add<Reverb>(simple_cast<double>(argv[value]));
		}
		if (strcmp(argv[arg], "Echo") == 0) {
			chain = chain.add<Echo>(simple_cast<double>(argv[value]));
		}
		if (strcmp(argv[arg], "Distortion") == 0) {
			chain = chain.add<Distortion>(simple_cast<std::int16_t>(argv[value]) * 100);
		}
		if (strcmp(argv[arg], "-v") == 0) {
			chain = chain.add<Volume>(simple_cast<std::int16_t>(argv[value]));
		}*/
		arg += 2;
		value += 2;
	}
	chain.add<DefaultSink>(device_id)
		.sink()
		->run();

}
catch (std::exception& e)
{
	using namespace iimavlib;
	logger[log_level::fatal] << "ERROR: An error occured during program run: " << e.what();
}





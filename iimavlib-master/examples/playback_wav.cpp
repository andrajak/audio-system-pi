/**
 * @file 	playback_high.cpp
 *
 * @date 	11.3.2013
 * @author 	Zdenek Travnicek <travnicek@iim.cz>
 * @copyright GNU Public License 3.0
 *
 * Simple example reading a wave file and playing it out using high level API.
 */


#include "iimavlib.h"
#include "iimavlib/WaveSink.h"
#include "iimavlib/WaveSource.h"
#include "iimavlib/filters/Echo.h"
#include "iimavlib/filters/Delay.h"
#include "iimavlib/filters/Volume.h"
#include "iimavlib/filters/Reverb.h"
#include "iimavlib/filters/Distortion.h"
#include "iimavlib/filters/Fuzz.h"
#include "iimavlib/filters/Overdrive.h"
#include "iimavlib/Utils.h"
#include <string>


int main(int argc, char** argv) try
{
	using namespace iimavlib;
	/* ******************************************************************
	 *                Process command line parameters
	 ****************************************************************** */
	if (argc<2) {
		logger[log_level::fatal] << "Not enough parameters. Specify the wave file please";
		return 1;
	}
	const std::string filename (argv[1]);
	logger[log_level::debug] << "Loading file " << filename;

	audio_id_t device_in = PlatformDevice::default_device();
	audio_id_t device_out = PlatformDevice::default_device();
	/*if (argc>2) {
		device_in = simple_cast<audio_id_t>(argv[2]);
	}*/
	std::string out_file;
	if (argc>2) 
		out_file = simple_cast<std::string>(argv[2]);
	/* ******************************************************************
	 *                Create and run the filter chain
	 ****************************************************************** */

	// dozvukovy casova oblast, odezva na impuls + bily sum
	// nelinearni, co to dela s sin a poslech
	// + spektrum pro vsechno

	auto chain = filter_chain<WaveSource>(filename)
						// DISTORTION + REVERB settings 
						/*.add<SimpleVolumeFilter>(50)
						.add<SimpleDistortionFilter>(3000)
						.add<Reverb1>(0.7, 0.6, 44100)
						.add<SimpleVolumeFilter>(200)*/
						
						// FUZZ settings
						/*.add<SimpleVolumeFilter>(50)
						.add<SimpleFuzzFilter>(1500, 1750)
						.add<SimpleDistortionFilter>(1750)
						.add<SimpleVolumeFilter>(200)*/

						// OVERDRIVE settings
						//.add<Volume>(10)
						.add<Reverb>(0, 0.7, 48000)
						//.add<Volume>(1000)

						//.add<Reverb>(0, 0.5)
					
						.add<WaveSink>(out_file)
						.add<DefaultSink>(device_in)
						.sink();
	chain->run();
}
catch (std::exception& e)
{
	using namespace iimavlib;
	logger[log_level::fatal] << "ERROR: An error occured during program run: " << e.what();
}

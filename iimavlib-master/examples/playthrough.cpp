#include "iimavlib.h"
#include "iimavlib/WaveSink.h"
#include "iimavlib/filters/NullFilter.h"
#include "iimavlib/filters/Echo.h"
#include "iimavlib/filters/Delay.h"
#include "iimavlib/filters/Volume.h"
#include "iimavlib/filters/Reverb.h"
#include "iimavlib/filters/Distortion.h"
#include "iimavlib/filters/Fuzz.h"
#include "iimavlib/filters/Overdrive.h"
#include "iimavlib/Utils.h"

int main(int argc, char** argv) try
{
	using namespace iimavlib;

	// if you want use default devices
	/*audio_id_t device_in = PlatformDevice::default_device();
	audio_id_t device_out = PlatformDevice::default_device();*/

	// input and output of HiFi audio card
	audio_id_t device_in = simple_cast<audio_id_t>("hw:2,0");
	audio_id_t device_out = simple_cast<audio_id_t>("hw:2,0");


	audio_params_t params(sampling_rate_t::rate_192kHz);

	// Create filter chain audio capture
	auto chain = filter_chain<PlatformSource>(params,device_in);
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "Echo") == 0) {
			chain = chain.add<Echo>(
				// level => decay
				simple_cast<double>(argv[++i]) / 100,
				// time
				simple_cast<double>(argv[++i]) / 100
				);
		}
		else if (strcmp(argv[i], "Delay") == 0) {
			chain = chain.add<Delay>(
				// level => decay
				simple_cast<double>(argv[++i]) / 100,
				// time
				simple_cast<double>(argv[++i]) / 100
				);
		}
		else if (strcmp(argv[i], "Distortion") == 0) {
			double disValue = simple_cast<double>(argv[++i]);
			// level => threshold
			chain = chain.add<Distortion>(500 - 4 * disValue);
			chain = chain.add<Volume>(100 + 4 * disValue);	
		}
		else if (strcmp(argv[i], "Fuzz") == 0) {
			double threshold = simple_cast<double>(argv[++i]);
			chain = chain.add<Fuzz>(
				// level => threshold
				500 - 4 * threshold,
				// fuzz => max value (threshold is mininum value)
				(500 - 4 * threshold) + simple_cast<double>(argv[++i])
				);
			chain = chain.add<Volume>(100 + 4 * threshold);
		}
		else if (strcmp(argv[i], "Overdrive") == 0) {
			chain = chain.add<Overdrive>(
				// level => threshold
				700 - 4 * simple_cast<double>(argv[++i])
				);
		}
		else if (strcmp(argv[i], "Reverb") == 0) {
			chain = chain.add<Reverb>(
				// time => bonus delay
				simple_cast<double>(argv[++i]),
				// wet
				simple_cast<double>(argv[++i]) / 100
				);
		}
	}					

	// And finally add audio sink
	auto sink = chain.add<PlatformSink>(device_out)
						.sink();

	assert(sink);
	sink->run();
	
}
catch (std::exception& e)
{
	using namespace iimavlib;
	logger[log_level::fatal] << "ERROR: An error occured during program run: " << e.what();
}
